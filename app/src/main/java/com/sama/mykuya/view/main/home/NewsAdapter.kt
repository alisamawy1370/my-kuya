package com.sama.mykuya.view.main.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.sama.mykuya.R
import com.sama.mykuya.model.News

class NewsAdapter(private val newsList :MutableList<News>,
                  private val mNewsClickEvent: NewsClickEvent? = null) : RecyclerView.Adapter<NewsAdapter.NewsViewHolder>() {

    fun updateItems(
        newNewsList: List<News>
    ) {
        this.newsList.clear()
        this.newsList.addAll(newNewsList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        return NewsViewHolder(
            (LayoutInflater.from(parent.context).inflate(R.layout.item_news, parent, false)))
    }

    override fun getItemCount(): Int = newsList.size

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        holder.bind(newsList[position])
    }

    inner class NewsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val mainLayout = itemView.findViewById<FrameLayout>(R.id.mainLayout)
        private val ivService = itemView.findViewById<ImageView>(R.id.ivService)
        private val tvTitle = itemView.findViewById<TextView>(R.id.tvTitle)
        private val tvMore = itemView.findViewById<TextView>(R.id.tvMore)
        private val tvDescription = itemView.findViewById<TextView>(R.id.tvDescription)

        init {
            tvMore.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    mNewsClickEvent?.onItemClicked(newsList[adapterPosition])
                }
            }
        }
        fun bind(news: News){

            with(news){
                tvTitle.text = title
                tvDescription.text = description


                val requestOptions = RequestOptions()
                requestOptions.placeholder(R.color.gray1)
                requestOptions.error(R.color.black1)

                Glide.with(itemView.context)
                    .load(image)
                    .apply(requestOptions)
                    .into(ivService)

            }
        }
    }

    interface NewsClickEvent{
        fun onItemClicked(news: News)
    }
}