package com.sama.mykuya.view.main.home

import com.sama.mykuya.model.Home
import com.sama.mykuya.view.base.BaseView


interface HomeView : BaseView {
    fun showHomeList(home: Home)
    fun showLoadingError(errMsg: String?)
}