package com.sama.mykuya.view.custom

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.transition.AutoTransition
import androidx.transition.TransitionManager
import com.sama.mykuya.R


class BottomTabItem : RelativeLayout{

    private var mainLayout: RelativeLayout? = null
    private var tvTitle: TextView? = null
    private var ivHeader: ImageView? = null
    private var ivBadge: ImageView? = null
    private var clickListener: OnClickListener? = null

    private var unselectedImage : Int? = null
    private var selectedImage : Int? = null

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init()
    }

    private fun init() {
         inflate(context, R.layout.view_tab_bottom_item, this)
         setupView()
    }

    private fun setupView() {
        tvTitle = findViewById(R.id.tvTitle)
        ivHeader = findViewById(R.id.ivHeader)
        ivBadge = findViewById(R.id.ivBadge)
        mainLayout = findViewById(R.id.mainLayout)
        mainLayout?.setOnClickListener{
            clickListener?.onClick(it)
        }
    }

    fun setText(text:String){
        tvTitle?.text = text
    }

    fun setUnselectedImage(@DrawableRes drawable : Int){
        this.unselectedImage = drawable
        ivHeader?.setImageResource(drawable)
    }

    fun setSelectedImage(@DrawableRes drawable : Int){
        this.selectedImage = drawable
    }

    fun selectItem(){
//        TransitionManager.beginDelayedTransition(mainLayout!!, AutoTransition().apply { duration = 200 })
//        tvTitle?.visibility = View.VISIBLE
        tvTitle?.setTextColor(ContextCompat.getColor(context, R.color.blue2))
        selectedImage?.let { ivHeader?.setImageResource(it) }
    }

    fun deselectItem(){
//        TransitionManager.beginDelayedTransition(mainLayout!!, AutoTransition().apply { duration = 50 })
//        tvTitle?.visibility = View.GONE
//        ivHeader?.setColorFilter(ContextCompat.getColor(context, R.color.gray1))
        tvTitle?.setTextColor(ContextCompat.getColor(context, R.color.gray1))
        unselectedImage?.let { ivHeader?.setImageResource(it) }
    }

    override fun setOnClickListener(l: OnClickListener?) {
        super.setOnClickListener(l)
        this.clickListener = l
    }

    fun showBadge(){
        ivBadge?.visibility = View.VISIBLE
    }

    fun hideBadge(){
        ivBadge?.visibility = View.GONE
    }
}