package com.sama.mykuya.view.map

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.sama.mykuya.R
import com.sama.mykuya.presenter.map.MapPresenter
import com.sama.mykuya.view.base.BaseActivity
import com.sama.mykuya.view.main.home.HomeFragment
import kotlinx.android.synthetic.main.activity_map.*
import org.koin.android.ext.android.inject
import java.util.*


class MapActivity : BaseActivity<MapView>(), MapView, OnMapReadyCallback {

    companion object {
        val MY_PERMISSIONS_REQUEST_LOCATION = 10002
    }

    override val presenter: MapPresenter by inject()
    override fun getLayout(): Int = R.layout.activity_map

    private var mMap: GoogleMap? = null
    private var locationButton: View? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.bind(this)

        setupViews()
    }

    private fun setupViews() {
        val mapFragment: SupportMapFragment? = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)

        locationButton = (mapFragment?.view?.findViewById<View>(1)
            ?.parent as View).findViewById(2)

        ibBack.setOnClickListener {
            onBackPressed()
        }

        ibMyLocation.setOnClickListener {
            locationButton?.callOnClick()
        }

        btnConfirm.setOnClickListener {
            val resultIntent = Intent().apply {
                putExtra(HomeFragment.LOCATION_NAME, tvLocationName.text)
            }
            setResult(Activity.RESULT_OK,resultIntent)
            finish()
        }
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap

        val currentLocation = LatLng(35.6892, 51.3890)
        mMap?.moveCamera(CameraUpdateFactory.newLatLng(currentLocation))

        mMap?.setOnCameraIdleListener{

            try {


                val geocoder = Geocoder(this, Locale.getDefault())
                val addresses: List<Address> =
                    geocoder.getFromLocation(
                        mMap?.cameraPosition?.target!!.latitude,
                        mMap?.cameraPosition?.target!!.longitude,
                        1
                    )
                tvLocationName.text = addresses[0].getAddressLine(0)
            } catch (e:Exception){
                tvLocationName.text = ""
            }
        }
        if (checkLocationPermission())
            locationPermissionGranted()
    }

    private fun checkLocationPermission(): Boolean {
        return if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            != PackageManager.PERMISSION_GRANTED
        ) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            ) {

                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    MY_PERMISSIONS_REQUEST_LOCATION
                )
            } else {
                ActivityCompat.requestPermissions(
                    this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    MY_PERMISSIONS_REQUEST_LOCATION
                )
            }
            false
        } else {
            true
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>, grantResults: IntArray
    ) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_LOCATION -> {
                if (grantResults.isNotEmpty()
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
                ) {

                    if (ContextCompat.checkSelfPermission(
                            this,
                            Manifest.permission.ACCESS_FINE_LOCATION
                        )
                        == PackageManager.PERMISSION_GRANTED
                    ) {
                        locationPermissionGranted()
                    }
                }
//                else {
//
//                }
                return
            }
        }
    }

    private fun locationPermissionGranted(){
        mMap?.isMyLocationEnabled = true
//        mMap?.uiSettings?.isMyLocationButtonEnabled = false
        locationButton?.visibility = View.GONE
    }
}