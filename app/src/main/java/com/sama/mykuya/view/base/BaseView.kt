package com.sama.mykuya.view.base


interface BaseView {
    fun showProgress(){}
    fun hideProgress(){}
}