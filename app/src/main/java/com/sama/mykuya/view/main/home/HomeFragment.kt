package com.sama.mykuya.view.main.home

import android.animation.LayoutTransition
import android.content.Intent
import android.content.res.Resources
import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.transition.AutoTransition
import androidx.transition.TransitionManager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.flexbox.FlexboxLayout
import com.google.android.flexbox.FlexboxLayoutManager
import com.sama.mykuya.R
import com.sama.mykuya.model.Home
import com.sama.mykuya.model.News
import com.sama.mykuya.model.Service
import com.sama.mykuya.presenter.home.HomePresenter
import com.sama.mykuya.view.base.BaseFragment
import com.sama.mykuya.view.custom.FullScreenLoadingState
import com.sama.mykuya.view.map.MapActivity
import kotlinx.android.synthetic.main.fragment_home.*
import org.koin.android.ext.android.inject


class HomeFragment : BaseFragment<HomeView>(),HomeView {

    companion object{
        val LOCATION_NAME = "LOCATION_NAME"
        val LOCATION_REQUEST = 10234
    }

    override val presenter : HomePresenter by inject()
    override fun getLayout(): Int = R.layout.fragment_home

    private var newsAdapter: NewsAdapter? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.bind(this)
        setupViews()
        presenter.loadHomeList()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        newsAdapter = null
    }

    private fun setupViews(){
        constraintLayout.layoutTransition
            .enableTransitionType(LayoutTransition.CHANGING)

        vFullScreenLoading.onRetryClick {
            presenter.loadHomeList()
        }

        llLocation.setOnClickListener {
            requireActivity().startActivityForResult(
                Intent(requireContext(),MapActivity::class.java),
                LOCATION_REQUEST
            )
        }

        ivArrow.setOnClickListener {
            onExpandableClicked()
        }

        newsAdapter =
            NewsAdapter(
                mutableListOf(),
                object :
                    NewsAdapter.NewsClickEvent {
                    override fun onItemClicked(news: News) {}
                })

        rvNews.apply {
            layoutManager = LinearLayoutManager(context,
                LinearLayoutManager.HORIZONTAL, false)
            adapter = newsAdapter
        }
    }

    override fun showHomeList(home: Home) {
        with(home){
            drawServiceItems(fbFeatures,features,3)
            drawServiceItems(fbServices,services,3)
            newsAdapter?.updateItems(news)
        }
    }

    override fun showLoadingError(errMsg: String?) {
        vFullScreenLoading?.setState(FullScreenLoadingState.ERROR, errMsg)
    }

    override fun showProgress() {
        vFullScreenLoading?.setState(FullScreenLoadingState.LOADING)
    }

    override fun hideProgress() {
        vFullScreenLoading?.setState(FullScreenLoadingState.DONE)
    }

    private var isExpanded = false
    private fun onExpandableClicked(){
        if (isExpanded){
            TransitionManager.beginDelayedTransition(constraintLayout, AutoTransition().apply { duration = 50 })
            val layoutParams = ConstraintLayout.LayoutParams(
                ConstraintLayout.LayoutParams.MATCH_PARENT,
                dpToPx(resources.getDimension(R.dimen.not_expanded_size)).toInt())
            layoutParams.topToBottom = cvFeatures.id
            layoutParams.startToStart = ConstraintLayout.LayoutParams.PARENT_ID
            layoutParams.endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
            layoutParams.setMargins(dpToPx(24f).toInt(),dpToPx(12f).toInt(),dpToPx(24f).toInt(),dpToPx(12f).toInt())
            fbServices.layoutParams = layoutParams

            ivArrow.setImageResource(R.drawable.ic_arrow_bottom_24)
            isExpanded = false
        }
        else{
            TransitionManager.beginDelayedTransition(constraintLayout!!, AutoTransition().apply { duration = 200 })
            val layoutParams = ConstraintLayout.LayoutParams(
                ConstraintLayout.LayoutParams.MATCH_PARENT,
                ConstraintLayout.LayoutParams.WRAP_CONTENT)
            layoutParams.topToBottom = cvFeatures.id
            layoutParams.startToStart = ConstraintLayout.LayoutParams.PARENT_ID
            layoutParams.endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
            layoutParams.setMargins(dpToPx(24f).toInt(),dpToPx(12f).toInt(),dpToPx(24f).toInt(),dpToPx(12f).toInt())
            fbServices.layoutParams = layoutParams

            ivArrow.setImageResource(R.drawable.ic_arrow_top_24)
            isExpanded = true
        }
    }

    private fun drawServiceItems(flexBox:FlexboxLayout, services: ArrayList<Service>, rowNumber:Int){
        flexBox.post {
            services.forEachIndexed { _, service->
                val view = LayoutInflater.from(context)
                    .inflate(R.layout.view_service, flexBox, false) as LinearLayout

                val layoutParams = FlexboxLayoutManager.LayoutParams(
                    flexBox.width / rowNumber,
                    dpToPx(resources.getDimension(R.dimen.service_item_height)).toInt())
                layoutParams.setMargins(0,dpToPx(8f).toInt(),0,dpToPx(8f).toInt())
                view.layoutParams = layoutParams

                val ivHeader = view.findViewById<ImageView>(R.id.ivHeader)
                val tvTitle = view.findViewById<TextView>(R.id.tvTitle)

                tvTitle.text = service.name

                val requestOptions = RequestOptions()
                requestOptions.placeholder(R.color.gray1)
                requestOptions.error(R.color.black1)

                Glide.with(requireContext())
                    .load(service.image)
                    .apply(requestOptions)
                    .into(ivHeader)
                flexBox.addView(view)
            }
        }

    }

    private fun dpToPx(dip : Float): Float {
        val r: Resources = resources
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            dip,
            r.displayMetrics
        )
    }

    fun updateLocation(location: String) {
        tvLocationName.text = location
    }
}