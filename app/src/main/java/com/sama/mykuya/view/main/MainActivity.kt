package com.sama.mykuya.view.main
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.sama.mykuya.R
import com.sama.mykuya.presenter.main.MainPresenter
import com.sama.mykuya.view.base.BaseActivity
import com.sama.mykuya.view.custom.BottomTabNavigation
import com.sama.mykuya.view.main.home.HomeFragment
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject

class MainActivity : BaseActivity<MainView>(),MainView {

    override val presenter : MainPresenter by inject()
    override fun getLayout(): Int = R.layout.activity_main

    private val homeFragment = HomeFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.bind(this)

        setupViews()
    }

    private fun setupViews(){
        loadHomeView()
        bottomTabNavigation.setItemList(listOf(
            Triple("Home",R.drawable.ic_home,R.drawable.ic_home_selected),
            Triple("MyJobs",R.drawable.ic_myjobs,R.drawable.ic_myjobs_selected),
            Triple("Support",R.drawable.ic_support,R.drawable.ic_support_selected),
            Triple("News",R.drawable.ic_news,R.drawable.ic_news_selected),
            Triple("Account",R.drawable.ic_profile,R.drawable.ic_profile_selected)))

        bottomTabNavigation.showTabBadge(1)
        bottomTabNavigation.bottomTabClickListener = object : BottomTabNavigation.BottomTabClickListener{
            override fun onItemClicked(index: Int, tabName: String) {
                //todo tab changed
            }

        }
    }

    private fun loadHomeView(){
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.contentContainer, homeFragment)
//        transaction.addToBackStack(HomeFragment.TAG)
        transaction.commit()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode){
            HomeFragment.LOCATION_REQUEST -> {
                if (resultCode == Activity.RESULT_OK) {
                    data?.getStringExtra(HomeFragment.LOCATION_NAME)?.let {
                        homeFragment.updateLocation(it)
                    }
                }
            }
        }
    }
}