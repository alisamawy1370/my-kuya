package com.sama.mykuya.view.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.sama.mykuya.presenter.base.BasePresenter

abstract class BaseActivity<T: BaseView> : AppCompatActivity(){

    protected abstract val presenter : BasePresenter<T>

    protected abstract fun getLayout() : Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayout())
    }

    override fun onDestroy() {
        presenter.dropView()
        super.onDestroy()
    }
}