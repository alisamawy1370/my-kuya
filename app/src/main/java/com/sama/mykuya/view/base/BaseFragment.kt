package com.sama.mykuya.view.base

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.sama.mykuya.presenter.base.BasePresenter

abstract class BaseFragment<T: BaseView> : Fragment(){

    protected abstract val presenter : BasePresenter<T>

    protected abstract fun getLayout() : Int

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(getLayout(), container, false)
    }

//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//    }

    fun Intent?.launchActivity() {
        if (this != null) {
            (activity as? BaseActivity<*>)?.startActivity(this)
        }
    }

    override fun onDestroyView() {
        presenter.dropView()
        super.onDestroyView()
    }
}