package com.sama.mykuya.di
import com.sama.mykuya.model.repo.HomeRepository
import com.sama.mykuya.model.repo.SimulateHomeClient
import org.koin.dsl.module

val repositoryModule = module {
    factory<HomeRepository>{ SimulateHomeClient() }
}