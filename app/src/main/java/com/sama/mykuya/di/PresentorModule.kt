package com.sama.mykuya.di
import com.sama.mykuya.presenter.home.HomePresenter
import com.sama.mykuya.presenter.home.HomePresenterImp
import com.sama.mykuya.presenter.main.MainPresenter
import com.sama.mykuya.presenter.main.MainPresenterImp
import com.sama.mykuya.presenter.map.MapPresenter
import com.sama.mykuya.presenter.map.MapPresenterImp
import org.koin.dsl.module

val presenterModule = module {
    factory<HomePresenter>{ HomePresenterImp(get()) }
    factory<MainPresenter>{ MainPresenterImp() }
    factory<MapPresenter>{ MapPresenterImp() }
}