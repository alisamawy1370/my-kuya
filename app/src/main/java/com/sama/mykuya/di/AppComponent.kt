package com.sama.mykuya.di

val appComponent = listOf(
    presenterModule,
    repositoryModule
)
