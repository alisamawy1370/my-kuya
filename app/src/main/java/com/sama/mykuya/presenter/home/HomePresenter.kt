package com.sama.mykuya.presenter.home

import com.sama.mykuya.presenter.base.BasePresenter
import com.sama.mykuya.view.main.home.HomeView

interface HomePresenter : BasePresenter<HomeView>{
    fun loadHomeList()
}