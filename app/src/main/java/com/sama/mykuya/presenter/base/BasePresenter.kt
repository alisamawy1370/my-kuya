package com.sama.mykuya.presenter.base

import com.sama.mykuya.view.base.BaseView

interface BasePresenter <in T: BaseView>{
    val view : BaseView?
    fun dropView()
    fun bind(view: T)
}