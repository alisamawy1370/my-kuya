package com.sama.mykuya.presenter.home

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer
import com.sama.mykuya.model.Home
import com.sama.mykuya.model.Resource
import com.sama.mykuya.model.repo.HomeRepository
import com.sama.mykuya.view.main.home.HomeView

class HomePresenterImp(private val repository: HomeRepository) : HomePresenter {

    private val homeDataSuccessLiveData = MediatorLiveData<Resource<Home>>()

    override var view: HomeView? = null
    private var observer : Observer<Resource<Home>>
    init {
        observer = Observer{
            when(it.status){
                Resource.Status.SUCCESS ->{
                    it.data?.let { it1 -> view?.showHomeList(it1) }
                }
                Resource.Status.ERROR ->{
                    view?.showLoadingError(it.error?.errorMessage)
                }
            }
        }
    }

    override fun bind(view: HomeView) {
        this.view = view
        homeDataSuccessLiveData.observeForever(observer)
    }

    override fun loadHomeList() {
        view?.showProgress()
        homeDataSuccessLiveData.addSource(repository.getHome()){
            homeDataSuccessLiveData.value = it
            view?.hideProgress()
        }
    }

    override fun dropView() {
        view = null
        homeDataSuccessLiveData.removeObserver(observer)
    }

}