package com.sama.mykuya.presenter.main

import com.sama.mykuya.view.main.MainView

class MainPresenterImp() : MainPresenter {

    override var view: MainView? = null

    override fun bind(view: MainView) {
        this.view = view
    }

    override fun dropView() {
        view = null
    }

}