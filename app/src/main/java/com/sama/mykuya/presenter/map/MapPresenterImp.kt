package com.sama.mykuya.presenter.map

import com.sama.mykuya.view.map.MapView

class MapPresenterImp() : MapPresenter {

    override var view: MapView? = null

    override fun bind(view: MapView) {
        this.view = view
    }

    override fun dropView() {
        view = null
    }

}