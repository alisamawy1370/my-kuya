package com.sama.mykuya.presenter.map

import com.sama.mykuya.presenter.base.BasePresenter
import com.sama.mykuya.view.map.MapView

interface MapPresenter : BasePresenter<MapView>{
}