package com.sama.mykuya.presenter.main

import com.sama.mykuya.presenter.base.BasePresenter
import com.sama.mykuya.view.main.MainView

interface MainPresenter : BasePresenter<MainView>{
}