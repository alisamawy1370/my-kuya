package com.sama.mykuya.model

data class Home(var features:ArrayList<Service>,
                 var services:ArrayList<Service>,
                 var news:ArrayList<News>)