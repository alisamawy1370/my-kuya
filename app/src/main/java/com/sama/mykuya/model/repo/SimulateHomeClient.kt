package com.sama.mykuya.model.repo

import android.os.Handler
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.sama.mykuya.R
import com.sama.mykuya.model.Home
import com.sama.mykuya.model.News
import com.sama.mykuya.model.Resource
import com.sama.mykuya.model.Service
import com.sama.mykuya.model.error_handler.ErrorModel
import com.sama.mykuya.utils.GlobalFunctions


class SimulateHomeClient : HomeRepository {

    override fun getHome(): MutableLiveData<Resource<Home>> {

        val response = MutableLiveData<Resource<Home>>()

        Handler().postDelayed({
            try {
                response.value = Resource(Resource.Status.SUCCESS, GlobalFunctions.getMockedHome(), null)
            } catch (e: Exception) {
                response.value =
                    Resource(Resource.Status.ERROR, null, ErrorModel(e, 0, e.message ?: "", true))
            }
        }, 1500)

        return response
    }

}