package com.sama.mykuya.model

data class News (var id:Int, var title:String, var description:String, var image:String)