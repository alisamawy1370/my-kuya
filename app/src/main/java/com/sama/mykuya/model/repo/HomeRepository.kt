package com.sama.mykuya.model.repo

import androidx.lifecycle.MutableLiveData
import com.sama.mykuya.model.Home
import com.sama.mykuya.model.Resource

interface HomeRepository {
    fun getHome(): MutableLiveData<Resource<Home>>
}