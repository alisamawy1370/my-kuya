package com.sama.mykuya.utils

import com.sama.mykuya.R
import com.sama.mykuya.model.Home
import com.sama.mykuya.model.News
import com.sama.mykuya.model.Service
import java.util.ArrayList

object GlobalFunctions {

    fun getMockedHome(): Home = Home(
        features = getMockedFeatures(),
        services = getMockedServiceList(),
        news = getMockedNews()
    )

    fun getMockedServiceList() =
        arrayListOf(
            Service(1,"Cleaning","${getPackageAddress()}${R.drawable.cleaning}"),
            Service(2,"Event Assistant","${getPackageAddress()}${R.drawable.window}"),
            Service(3,"Office Assistant","${getPackageAddress()}${R.drawable.computer}"),
            Service(4,"Coffee Delivery","${getPackageAddress()}${R.drawable.coffe}"),
            Service(5,"Food Delivery","${getPackageAddress()}${R.drawable.food}"),
            Service(6,"Shopping","${getPackageAddress()}${R.drawable.shopping}"),
            Service(7,"Grocery Delivery","${getPackageAddress()}${R.drawable.groccery}"),
            Service(8,"Messenger","${getPackageAddress()}${R.drawable.mail}"),
            Service(9,"Bills Payment","${getPackageAddress()}${R.drawable.tax}"),
            Service(10,"Personal Assistant","${getPackageAddress()}${R.drawable.friends}"),
            Service(11,"Assistant on Bike","${getPackageAddress()}${R.drawable.post}"),
            Service(12,"Queuing Up","${getPackageAddress()}${R.drawable.election}"),
            Service(13,"Pet Sitting","${getPackageAddress()}${R.drawable.pet}"),
            Service(14,"Flyering","${getPackageAddress()}${R.drawable.papers}"),
            Service(15,"Dish Washing","${getPackageAddress()}${R.drawable.wash}"),
            Service(16,"Cash on Delivery","${getPackageAddress()}${R.drawable.pay}"),
            Service(17,"Massage","${getPackageAddress()}${R.drawable.massage}"),
            Service(18,"Deep Clean","${getPackageAddress()}${R.drawable.vacume}"),
            Service(19,"Car Wash and Wax","${getPackageAddress()}${R.drawable.carwash}"),
            Service(20,"Manicure and Pedicure","${getPackageAddress()}${R.drawable.makeup}")
        )

    fun getMockedFeatures(): ArrayList<Service> =
        arrayListOf(
            Service(1,"Cleaning","${getPackageAddress()}${R.drawable.cleaning}"),
            Service(6,"Shopping","${getPackageAddress()}${R.drawable.shopping}"),
            Service(17,"Massage","${getPackageAddress()}${R.drawable.massage}") )

    fun getMockedNews(): ArrayList<News> = arrayListOf(
        News(1,"How to use the app","Getting access to on-demand","${getPackageAddress()}${R.drawable.news_1}"),
        News(1,"List your services","Do you offer Massage?","${getPackageAddress()}${R.drawable.news_2}"))

    fun getPackageAddress() = "android.resource://com.sama.mykuya/"
}