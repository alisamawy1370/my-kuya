package com.sama.mykuya

import android.support.test.runner.AndroidJUnit4
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.sama.mykuya.view.main.MainActivity
import com.sama.mykuya.view.main.home.HomeFragment
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.util.regex.Pattern.matches


@RunWith(AndroidJUnit4::class)
class HomeListTest {

    @Rule
    var activityTestRule: ActivityTestRule<MainActivity> = ActivityTestRule(
        MainActivity::class.java
    )

    @Before
    fun registerIdlingResource() {
        IdlingRegistry.getInstance().register(EspressoTestingIdlingResource.idlingResource)
    }

    @After
    fun unregisterIdlingResource() {
        IdlingRegistry.getInstance().unregister(EspressoTestingIdlingResource.idlingResource)
    }

    @Test
    fun useAppContext() {
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("com.sama.MyKuya", appContext.packageName)
    }

    @Test
    fun checkStaticViewNavigation() {
        onView(withId(R.id.bottomTabNavigation)).check(matches(isDisplayed()))
    }

    @Test
    fun checkStaticViewContainer() {
        onView(withId(R.id.contentContainer)).check(matches(isDisplayed()))
    }

    @Test
    fun checkLoadedViewVisibility() {

        activityTestRule.activity.runOnUiThread { val homeFragment: HomeFragment = HomeFragment() }

        onView(withId(R.id.ivTopHeader)).check(matches(isDisplayed()))

        // verify recycler view is being displayed
        onView(withId(R.id.fbFeatures))
            .check(matches(isDisplayed()))
    }

}