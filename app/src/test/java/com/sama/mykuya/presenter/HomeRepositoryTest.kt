package com.sama.mykuya.presenter

import org.junit.Before
import com.sama.mykuya.model.Home
import com.sama.mykuya.model.Resource
import com.sama.mykuya.model.repo.HomeRepository
import com.sama.mykuya.presenter.home.HomePresenter
import com.sama.mykuya.presenter.home.HomePresenterImp
import com.sama.mykuya.view.main.home.HomeView

import org.mockito.ArgumentCaptor
import org.mockito.Mock
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito.verify
import org.mockito.Mockito.times
import org.mockito.MockitoAnnotations
import org.junit.Assert.assertTrue

@RunWith(JUnit4::class)
class HomeRepositoryTest {

    @Mock
    private lateinit var view: HomeView

    @Mock
    private lateinit var repository: HomeRepository

    private lateinit var presenter: HomePresenter

    @Before
    fun setUp(){
        MockitoAnnotations.initMocks(this)
        presenter = HomePresenterImp(repository).apply {
            bind(this@HomeRepositoryTest.view)
        }
    }

    @Test
    fun testLoadSuccessHomeData(){
        val resourceValue = repository.getHome()
        Thread.sleep(2000)
        val argumentCaptor : ArgumentCaptor<Resource<*>> =
            ArgumentCaptor.forClass(Resource::class.java)
        verify(resourceValue, times(1)).value = argumentCaptor.capture() as Resource<Home>?

        assertTrue(resourceValue.value?.status == Resource.Status.SUCCESS)
    }

    @Test
    fun testLoadFeatureSizeHomeData(){
        val resourceValue = repository.getHome()
        Thread.sleep(2000)
        val argumentCaptor : ArgumentCaptor<Resource<*>> =
            ArgumentCaptor.forClass(Resource::class.java)
        verify(resourceValue, times(1)).value = argumentCaptor.capture() as Resource<Home>?

        assertTrue(resourceValue.value?.data?.features?.size == 3)
    }

}