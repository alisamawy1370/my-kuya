import org.jetbrains.kotlin.kapt3.base.Kapt

plugins {
    id(BuildPlugins.androidApplication)
    id(BuildPlugins.kotlinAndroid)
    id(BuildPlugins.kotlinAndroidExtensions)
    id(BuildPlugins.kotlinKapt)
}

android {
    compileSdkVersion(AndroidSdk.compile)
    defaultConfig {
        applicationId = AndroidSdk.applicationId
        minSdkVersion(AndroidSdk.min)
        targetSdkVersion(AndroidSdk.target)
        versionCode = AndroidSdk.versionCode
        versionName = AndroidSdk.versionName
        testInstrumentationRunner = AndroidSdk.testInstrumentRunner
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }

    buildTypes {
        getByName("debug"){
            isDebuggable = true
            buildConfigField("String","BASE_URL","\"https://api-dot-rafiji-staging.appspot.com/customer/\"")
        }
        getByName("release") {
            isMinifyEnabled = false
            buildConfigField("String","BASE_URL","\"https://api-dot-rafiji-staging.appspot.com/customer/\"")
        }
    }
}


dependencies {
    implementation(Libraries.kotlinStdLib)
    implementation(Libraries.ktxCore)
    implementation(Libraries.appCompat)
    implementation(Libraries.design)
    implementation(Libraries.constraintLayout)
    implementation(Libraries.recyclerview)
    implementation(Libraries.cardview)
    implementation(Libraries.flexBox)

    implementation(Libraries.androidxLifeCycleViewModel)

    implementation(Libraries.kotlinCoroutineCore)
    implementation(Libraries.kotlinCoroutineAndroid)

//    implementation(Libraries.retrofit)
//    implementation(Libraries.retrofitGsonConverter)
//    implementation(Libraries.gson)
//    implementation(Libraries.loggingInterceptor)

    implementation(Libraries.koin)
    implementation(Libraries.koinScope)
//    implementation(Libraries.koinViewModel)

    implementation(Libraries.glide)
    kapt(Libraries.glideCompiler)

    implementation(Libraries.playServicesMap)

//    implementation(Libraries.lifeCycleExtensions)
//    implementation(Libraries.lifeCycleViewModel)
//    implementation(Libraries.lifeCycleLiveData)

    androidTestImplementation(Libraries.junit)
    androidTestImplementation(Libraries.testRunner)
//    androidTestImplementation (Libraries.lifeCycleTest)
//    androidTestImplementation (Libraries.lifeCycleCompiler)
//    androidTestImplementation(Libraries.mockWebserver)
    androidTestImplementation(Libraries.koinTest)
    testImplementation(Libraries.mockitoAll)
    testImplementation(Libraries.hamcrestAll)
    androidTestImplementation(Libraries.mockitoCore)

    androidTestImplementation(Libraries.espressoCore)
    androidTestImplementation(Libraries.espressoContrib)
    androidTestImplementation(Libraries.espressoIntents)
    androidTestImplementation(Libraries.espressoConcurrent)
}
